#!flask/bin/python
from os import abort

from flask import Flask, jsonify, request
from shapely.geometry import Polygon, Point

app = Flask(__name__)

pdvs = [
    {
        "id": 1,
        "tradingName": "1 Adega da Cerveja - Pinheiros",
        "ownerName": "Zé da Silva",
        "document": "1432132123891/0001",
        "coverageArea": {
            "type": "MultiPolygon",
            "coordinates": [
                [[[30, 20], [45, 40], [10, 40], [30, 20]]],
                [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
            ]
        },
        "address": {
            "type": "Point",
            "coordinates": [-46.57421, -21.785741]
        }
    },
    {
        "id": 2,
        "tradingName": "2 Porto Seguro",
        "ownerName": "Zé da Silva",
        "document": "1432132123891/0001",
        "coverageArea": {
            "type": "MultiPolygon",
            "coordinates": [
                [[[30, 20], [45, 40], [10, 40], [30, 20]]],
                [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
            ]
        },
        "address": {
            "type": "Point",
            "coordinates": [-46.57421, -21.785741]
        }
    },
    {
        "id": 3,
        "tradingName": "3 Arenas Blancas",
        "ownerName": "Zé da Silva",
        "document": "1432132123891/0001",
        "coverageArea": {
            "type": "MultiPolygon",
            "coordinates": [
                [[[30, 20], [45, 40], [10, 40], [30, 20]]],
                [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
            ]
        },
        "address": {
            "type": "Point",
            "coordinates": [-46.57421, -21.785741]
        }
    }
]


@app.route('/todo/api/v1.0/pdvs', methods=['GET'])
def get_pdvs():
    return jsonify({'pdvs': pdvs})


@app.route('/todo/api/v1.0/pdvs/<int:pdv_id>', methods=['GET'])
def get_pdv(pdv_id):
    pdv = [pdv for pdv in pdvs if pdv['id'] == pdv_id]
    if len(pdv) == 0:
        abort(404)
    return jsonify({'pdv': pdv[0]})


@app.route('/todo/api/v1.0/pdvs/busca/')
def my_route():
    longitud = request.args.get('longitud', default=1, type=float)
    latitud = request.args.get('latitud', default=1, type=float)
    p1 = Point(longitud, latitud)
    idpartner = 0
    for pdv in pdvs:
        coords = pdv['coordinates']
        poly = Polygon(coords)
        if poly.contains(p1):
            return jsonify({'pdv': pdv[0]})
        else:
            return jsonify(error='No hay partner cerca')


@app.route('/todo/api/v1.0/pdvs/agrega/', methods=['POST'])
def put_pdv():
    content = request.get_json(force=True)

    if 'tradingName' in content.keys():
        tradingName = content['tradingName']
    else:
        return jsonify(error='json no es correcto')
    if 'ownerName' in content.keys():
        ownerName = content['ownerName']
    else:
        return jsonify(error='json no es correcto')
    return jsonify('ingresado Partner ' + tradingName + ownerName)


if __name__ == '__main__':
    app.run(debug=True)
